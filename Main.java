/**	The main class of the application, used to run all the methods.
 * 
 * 	@author Daniel O'Connell
 */

public class Main {

	static Db db = null;
	
	/**	Sequentially call the methods from the Db class.
	 * 
	 * 	@param args 
	 */
	public static void main(String[] args) {
		db = new Db();
		
		db.getConnection("");
		db.createDB("schemastore");
		db.getConnection("schemastore");
		db.createTable();
		
		db.getConnection("university");
		db.getSchemaInfo();
		
		db.getConnection("lsh");
		db.getSchemaInfo();
		
		db.getConnection("schemastore");
		db.debugPrint();
		db.fillTable();
		db.close();
	}

}
