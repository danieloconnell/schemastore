drop database if exists university;
create database university;
use university;

create table department (d_id varchar(5), d_title varchar(10), location varchar(15), primary key (d_id)) engine = InnoDB;

create table staff (s_id varchar(4), initials varchar (4), s_name varchar(15), pos varchar(15), qual varchar(5), d_id varchar(5), primary key (s_id),
	foreign key (d_id) references department(d_id)
) engine = InnoDB;


create table courses (c_id varchar(3), c_title varchar(35), code varchar(4), year varchar(4), d_id varchar(5), primary key (c_id),
	foreign key (d_id) references department(d_id)
) engine = InnoDB;

create table projects (p_id varchar(10), p_title varchar(30), funder varchar(10), funding int, primary key (p_id)) engine = InnoDB;

create table give_course (s_id varchar(4), c_id varchar(3), primary key (s_id, c_id),
	foreign key (s_id) references staff (s_id),
	foreign key (c_id) references courses (c_id)

) engine = InnoDB;

create table work_on (s_id varchar(4), p_id varchar(10), start_date int, stop_date int, primary key (s_id, p_id),
	foreign key (s_id) references staff (s_id),
	foreign key (p_id) references projects( p_id)
) engine = InnoDB ;

show tables;

insert into department values ("COMP","Computing","InfoLab");
insert into department values ("NEXT","Nextology","George Fox");

insert into staff values("PS","P.H.","Sawyer","Senior Lecturer","PhD","COMP");

insert into staff values("TR","T.","Rodden","Professor","PhD","COMP");
insert into staff values("JAM","J.A.","Mariani","Senior Lecturer","PhD","COMP");
insert into staff values("GSB","G.S.","Blair","Professor","PhD","COMP");
insert into staff values("BB","B.","Bear","Professor","BA","NEXT");

insert into courses values("UID","User Interface Design","361","3rd","COMP");
insert into courses values("IOS", "Introduction to Operating Systems", "112c", 
				"1st", "COMP");
insert into courses values("DB","Databases","242","2nd","COMP");
insert into courses values("PA","Programming in Ada","111a","1st","COMP");
insert into courses values("BN","Basic Nextology","110","1st","NEXT");

insert into projects values("COMIC","COMIC","ESPRIT",1200);
insert into projects values("OSCAR","OSCAR","SERC", 200);
insert into projects values("UIOODB","UI Development for OODBs","SERC", 150);
insert into projects values("MCSCW","Multimedia and CSCW","SERC", 100);
insert into projects values("AN","Advanced Nextology","NERC", 500);

insert into give_course values("PS","UID");
insert into give_course values("TR","UID");
insert into give_course values("GSB","IOS");
insert into give_course values("JAM","DB");
insert into give_course values("JAM","PA");
insert into give_course values("BB","BN");

insert into work_on values("TR","COMIC",1991,1994);
insert into work_on values("JAM","COMIC",1992,1994);
insert into work_on values("JAM","OSCAR",1989,1991);
insert into work_on values("PS","UIOODB",1993,1994);
insert into work_on values("GSB","MCSCW",1990,1994);
insert into work_on values("BB","AN",1985,1989);

quit

