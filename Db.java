/**	This class holds all of the methods for interacting with the databases.	
 * 
 * 	@author Daniel O'Connell
 */

import java.sql.*;

public class Db {

	static final String DRIVER = "com.mysql.jdbc.Driver";
    static final String URL    = "jdbc:mysql://localhost/";
    
    private Connection con = null;
	private ResultSet rs = null;
	private Statement st = null;
	private DatabaseMetaData dmd = null;
	private String[][] values = new String[40][9];
	
	/**	This is class constructor which loads the JDBC driver for the application.
	 * 
	 * 	@exception cnfe Caught if the driver cannot be loaded
	 */
	Db ()
    {
        try {
            Class.forName( DRIVER ); // loads the JDBC driver
        } catch ( ClassNotFoundException cnfe ) {
        	System.out.println("Database Connection error: " + cnfe);
        };
    }
	
	/**	This method is used to make the exception handling process easier. A message is 
	 * 	printed, the stack trace is shown and the application exits.
	 * 
	 * 	@param message User friendly message to show where the application has crashed
	 * 	@param e The exception thrown by the error
	 */
	private void notify(String message, Exception e)
	{
		System.out.println(message+" : "+e);
		e.printStackTrace();
		System.exit(1);
	}
	
	/**	This method is used to connect to the database, given as a parameter, and return 
	 * 	a connection object so that it can be used. 
	 * 
	 * 	@param name Name of the database to get a connection to
	 * 	@return Connection Connection object for the database
	 * 	@exception SQLException Caught if the connection or DriverManager method fails
	 * 	@see Connection
	 */
	public Connection getConnection(String name)
	{
		try {
	    	con = DriverManager.getConnection(URL+name, "root", "password");
	        con.setAutoCommit(false);
	    } catch (SQLException sqle) {
	        notify("Connection error", sqle);
	    };

	    System.out.println("getConnection URL= ["+URL+name+"]");

	    return con;
 	}
	
	/**
	 * 	@param dbname Name of the database to create
	 * 	@exception SQLException Caught if either of the queries or connection statement fails
	 */
	public void createDB(String dbname)
	{
		try {
			st = con.createStatement();
			st.executeUpdate("DROP DATABASE IF EXISTS " + dbname);
			st.executeUpdate("CREATE DATABASE " + dbname);
		} catch (SQLException e) {
			notify("Database creation error", e);
		}
		
		System.out.println("Database created");
	}
	
	/**	This method is used to create the table in the database that it is connected to at
	 * 	the time. A short message is given at the end to let you know that it completed
	 * 	successfully.
	 * 
	 * 	@exception SQLException Caught if the query or connection method fails
	 */
	public void createTable()
	{
		try {
			st = con.createStatement();
			st.executeUpdate("CREATE TABLE schemainfo (`database` varchar(10) NOT NULL," 
													+ " `relname` varchar(15) NOT NULL," 
													+ " `fieldname` varchar(15) NOT NULL,"
													+ " `type` varchar(25) NOT NULL," 
													+ " `size` int(10) NOT NULL,"
													+ " `primarykey` enum('yes', 'no') DEFAULT 'no',"
													+ " `foreignkey` enum('yes', 'no') DEFAULT 'no',"
													+ " `fktable` varchar(15),"
													+ " `fkfield` varchar(15),"
													+ " PRIMARY KEY( `database`, `relname`, `fieldname`)) ENGINE = InnoDB");
		} catch (SQLException e) {
			notify("Table creation error", e);
		}
		
		System.out.println("Table created");
	}
	
	/**	This method creates the DatabaseMetaData object from the connection to the local
	 * 	MySQL server and then uses a list of table names held in the ResultSet object to
	 * 	loop through the databases and collect all of the schema information. This 
	 * 	information is then held in the values[][] array.
	 * 
	 * 	@exception SQLException Caught if any of the queries, connection methods, ResultSet
	 * 							methods or MetaData methods fail
	 */
	int i,k = 0;
	public void getSchemaInfo()
	{
		try {
			dmd = con.getMetaData();
			rs = dmd.getTables(null, null, null, new String[] {"TABLE"});
			
			while(rs.next())
			{
				ResultSet res = dmd.getColumns(null, null, rs.getString("TABLE_NAME"), null);
				while(res.next())
				{
					values[i][0] = rs.getString(1);
					values[i][1] = rs.getString("TABLE_NAME");
					values[i][2] = res.getString("COLUMN_NAME");
					values[i][3] = res.getString("TYPE_NAME");
					values[i][4] = res.getString("COLUMN_SIZE");
					
					ResultSet key = dmd.getExportedKeys(null, null, rs.getString("TABLE_NAME"));
					while(key.next())
					{	
						if(values[i][2].equals(key.getString("PKCOLUMN_NAME")))
						{
							values[i][5] = "1";
						}
						
						if(values[i][2].equals(key.getString("FKCOLUMN_NAME")))
						{
							values[i][6] = "1";
							values[i][7] = key.getString("FKTABLE_NAME");
							values[i][8] = key.getString("FKCOLUMN_NAME");
						}
					}
					i++;
				}
				
			}
			
			System.out.println("Schema info gathered");
		} catch (SQLException e) {
			notify("Schema error", e);
		}	
	}
	
	public void debugPrint()
	{	
		for(int k=0; k<31; k++)
		{
			for(int j=0; j<9; j++)
			{
				System.out.print(values[k][j] + ", ");
			}
			System.out.println("\n");
		}
	}
	
	/**	This method loops through the data held in the values[][] array and inserts each
	 * 	row into a row of the "schemainfo" table. A short message is given at the end to 
	 * 	let you know that it completed successfully.
	 * 
	 * 	@exception SQLException Caught if the "INSERT" query or createStatement() fails
	 */
	public void fillTable()
	{
		try {
			st = con.createStatement();
			for(int n=0; n<31; n++)
			{
				String m = "no", o = "no";
				if(values[n][5]=="1")
					m = "yes";
				
				if(values[n][6]=="1")
					o = "yes";

				
				st.executeUpdate("INSERT INTO schemainfo VALUES ('" + values[n][0] + "', '"
																	+ values[n][1] + "', '"
																	+ values[n][2] + "', '"
																	+ values[n][3] + "', '"
																	+ values[n][4] + "', '"
																	+ m + "', '"
																	+ o + "', '" 
																	+ values[n][7] + "', '"
																	+ values[n][8] + "')");
			}
			con.commit();
		} catch (SQLException e) {
			notify("Insertion error", e);
		}
		
		System.out.println("Schemainfo filled");
	}
	
	/**	
	 * 	This method is used to close the connection to the local MySQL server and free
	 * 	up the resources that were in use. After this the application exits. 
	 * 
	 * 	@exception SQLException Caught if the connection fails to close
	 */
	public void close()
	{
		try {
			con.close();
			System.exit(0);
		} catch (SQLException e) {
			notify("Close connection error", e);
		}
	}
}
