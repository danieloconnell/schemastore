
use lsh


LOAD DATA LOCAL INFILE 'planets.txt'
INTO TABLE planets
  FIELDS TERMINATED BY '\t' lines terminated by '\r\n';
select "finished loading planets";


LOAD DATA LOCAL INFILE 'heroes.txt'
INTO TABLE heroes
  FIELDS TERMINATED BY '\t' lines terminated by '\r\n';
select "finished loading heroes";


LOAD DATA LOCAL INFILE 'powers.txt'
INTO TABLE powers
  FIELDS TERMINATED BY '\t' lines terminated by '\r\n';
select "finished loading powers";


quit
