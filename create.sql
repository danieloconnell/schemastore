drop database if exists lsh;
create database lsh;
use lsh;

create table planets (name varchar(15), popvalue integer, primary key(name)) engine = InnoDB;

create table heroes(codename varchar(30), secretIdentity varchar(30), homeWorld varchar(15), primary key(codename),
foreign key (homeWorld) references planets (name)	
) engine = InnoDB;

create table powers(codename varchar(30), description varchar (100), primary key (codename, description),
foreign key (codename) references heroes(codename)
) engine = InnoDB;

show tables;

quit
